// import { cpus } from 'os';

const moment = require('moment');
const JSPdf = require('jspdf');

const keys = [
    'Jour',
    'Date',
    'Page de départ',
    'Objectif',
    'Objectif atteint',
    'Progression'
];

const rt = {
    getProgression(pageActuelle, pageDepart, totalPages) {
        const pourcentage =
            ((pageActuelle - pageDepart + 1) * 100) / totalPages;
        return pourcentage.toFixed(2) + ' %';
    },

    generateTable(json) {
        const cols = [];
        let i, j, k;

        // Push all keys to the array
        for (k in json[0]) {
            cols.push(k);
        }

        const table = document.querySelector('#tableRoutine');

        // Create table row tr element of a table
        const tr = table.insertRow(-1);

        for (i = 0; i < keys.length; i++) {
            // Create the table header th element
            const theader = document.createElement('th');
            theader.innerHTML = keys[i];

            // Append columnName to the table row
            tr.appendChild(theader);
        }

        // Adding the data to the table
        for (i = 0; i < json.length; i++) {
            // Create a new row
            const trow = table.insertRow(-1);
            for (j = 0; j < cols.length; j++) {
                const cell = trow.insertCell(-1);

                // Inserting the cell at particular place
                cell.innerHTML = json[i][cols[j]];
            }
        }
    },
    calculerRoutine(
        dateDepart,
        pageDepart,
        pageFin,
        nbPagesParJour,
        totalPages
    ) {
        let jour = 1;
        let date = moment(dateDepart);
        let pageActuelle = pageDepart;
        const jsonData = [];

        while (pageActuelle < pageFin) {
            const objectif = Math.min(
                pageActuelle + nbPagesParJour - 1,
                pageFin
            );
            const progression = rt.getProgression(
                objectif,
                pageDepart,
                totalPages
            );

            const obj = {};
            obj.jour = jour;
            obj.date = date.format('DD/MM/YYYY');
            obj.pageDepart = pageActuelle;
            obj.objectif = objectif;
            obj.objectifAtteint = '';
            obj.progression = progression;

            jsonData.push(obj);

            jour += 1;
            date = moment(date).add(1, 'days');
            pageActuelle = objectif + 1;
        }

        return JSON.parse(JSON.stringify(jsonData));
    },

    clearTable() {
        document.querySelector('table').innerHTML = '';
    },

    construireRoutine(
        dateDepart,
        pageDepart,
        pageFin,
        nbPagesParJour,
        totalPages
    ) {
        const routine = rt.calculerRoutine(
            dateDepart,
            pageDepart,
            pageFin,
            nbPagesParJour,
            totalPages
        );
        rt.clearTable();
        rt.generateTable(routine);
    },

    exportPDF(nomLivre) {
        const baseFilename = 'RoutineDeLecture.pdf';

        // prettier-ignore
        const filename = nomLivre ?
            nomLivre + ' - ' + baseFilename :
            baseFilename;

        // prettier-ignore
        const title = nomLivre ?
            nomLivre + ' - Routine de lecture' :
            'Routine de lecture';

        const doc = new JSPdf('p', 'pt', 'a4');
        doc.text(title, doc.internal.pageSize.getWidth() / 2, 20, {
            align: 'center'
        });
        doc.autoTable({
            tableLineColor: [0, 0, 0],
            tableLineWidth: 1,
            styles: {
                lineColor: [0, 0, 0],
                lineWidth: 1,
                halign: 'center',
                valign: 'middle'
            },
            headStyles: {
                fontSize: 12,
                fontStyle: 'normal',
                fillColor: [255, 255, 255],
                textColor: [0, 0, 0]
            },
            html: '#tableRoutine'
        });
        doc.save(filename);
    }
};

// eslint-disable-next-line prettier/prettier
export default rt;